//Bibliotecas utilizadas na implementacao
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>

//Defines para tornar a implementacao mais facil
#define N 5 //numero de filosofos (threads)
#define PENSAR 0 //0 ? o numero relativo ao estado PENSAR
#define FOME 1 //1 ? o numero relativo ao estado FOME
#define COMER 2 //2 ? o numero relativo ao estado COMER
#define ESQUERDA (nfilosofo+4)%N //agarrar garfo da esquerda
#define DIREITA (nfilosofo+1)%N  //agarrar garfo da direita

//Funcoes necessarias para a resolucao do problema
void *filosofo(void *num); //threads
void agarraGarfo(int);
void deixaGarfo(int);
void testar(int);

//Declaracao de funcoes que serao necessarias para a manipulacao dos semaforos
int sem_init(sem_t *sem, int pshared, unsigned int value); //semaforo inicialiazado com "value"
int sem_wait(sem_t * sem); //bloqueia o semaforo
int sem_post(sem_t * sem); //desbloqueia o semaforo

//Variaveis globais
sem_t mutex; //um mutex para o programa em geral
sem_t S[N]; //inicializacao do sem?foro
int estado[N]; //guarda o estado de cada filosofo
int nfilosofo[N]={0,1,2,3,4};

int main() {
    //cria outro processo pesado
    fork();

    int i;
    pthread_t thread_id[N]; //identificadores das threads

    //inicilizando o mutex
    sem_init(&mutex,0,1);

    //inicializando os semaforos de cada filosofo (de cada thread)
    for(i=0;i<N;i++)
        sem_init(&S[i],0,0);

    //criando as threads
    for(i=0;i<N;i++) {
        pthread_create(&thread_id[i], NULL, filosofo, &nfilosofo[i]);
        printf("Filosofo %d esta pensando.\n",i+1);
    }

    //fazendo a juncao das threads
    for(i=0;i<N;i++)
        pthread_join(thread_id[i],NULL);

    return(0);
}

//  Essa funcao ? chamada na pthread_create, para indicar ? funcao qual sera thread realizada.
//  Cada thread possui um identificador -> thread_id[]
//  E tambem um vetor auxiliar para identificar os estados de cada uma -> nfilosofo[]
void *filosofo(void *num)
{
    while(1)
    {
        int *i = num;
        sleep(1);
        agarraGarfo(*i);
        sleep(1);
        deixaGarfo(*i);
    }
}

//  Funcao para agarrar os garfos. Nela, passa-se o identificador do filosofo (da thread)
//  Altera o estado do filosofo para FOME
//  O filosofo testa se pode pegar o garfo
//  Caso possa, ele poder? comer
//  Caso n?o, ele ? posto para dormir
void agarraGarfo(int nfilosofo)
{
    sem_wait(&mutex); //Avisando ao mutex que os garfos estarao em um estado critico
    estado[nfilosofo] = FOME;
    printf("Filosofo %d esta com fome.\n", nfilosofo+1);
    testar(nfilosofo);
    sem_post(&mutex);
    sem_wait(&S[nfilosofo]);
    sleep(1);
}

//  Funcao para verificar se o filosofo pode ou nao deixar os garfos
void deixaGarfo(int nfilosofo)
{
    sem_wait(&mutex); //Avisando ao mutex que os garfos estarao em um estado critico
    estado[nfilosofo] = PENSAR;
    printf("Filosofo %d deixou os garfos %d e %d.\n", nfilosofo+1, ESQUERDA+1, nfilosofo+1);
    printf("Filosofo %d esta pensando.\n", nfilosofo+1);
    testar(ESQUERDA); //Acao critica
    testar(DIREITA); //Acao critica
    sem_post(&mutex); //Avisando ao mutex que os garfos nao estao mais em um estado critico
}

//  Funcao para validar se o filosofo pode comer
void testar(int nfilosofo)
{
    if(estado[nfilosofo]==FOME && estado[ESQUERDA]
                                  != COMER && estado[DIREITA]!= COMER)
    {
        estado[nfilosofo] = COMER;
        sleep(2);
        printf("Filosofo %d agarrou os garfos %d e %d.\n", nfilosofo+1, ESQUERDA+1, nfilosofo+1);
        printf("Filosofo %d esta comendo.\n", nfilosofo+1);
        sem_post(&S[nfilosofo]);
    }
}

